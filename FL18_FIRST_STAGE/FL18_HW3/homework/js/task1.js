const MIN_AMOUNT = 1000;
const MAX_PERCENT = 100;
const HUNDRED = 100;

const amountOfMoney =
    parseInt(prompt('Please, enter initial amount of money'));
const numberOfYears =
    parseInt(prompt('Please, enter number of years'));
const percentageOfYearString =
    prompt('Please, enter percentage of a year');
const percentageOfYear =
    parseFloat(percentageOfYearString.replace(',', '.'));

let totalAmount = 0;
let totalProfit = 0;

function isFloat(n) {
  return n === +n && n !== (n | 0);
}

function isInteger(n) {
  return n === +n && n === (n | 0);
}

if (isFloat(amountOfMoney) || !isInteger(amountOfMoney) ||
    amountOfMoney === null
    || amountOfMoney < MIN_AMOUNT) {
  alert('Invalid input data');
} else {
  if (isFloat(numberOfYears) || !isInteger(numberOfYears) ||
      numberOfYears === null || numberOfYears < 1) {
    alert('Invalid input data');
  } else {
    if (!Number.isFinite(percentageOfYear) ||
        percentageOfYear === null ||
        percentageOfYear < 1 || percentageOfYear > MAX_PERCENT) {
      alert('Invalid input data');
    } else {
      totalAmount = amountOfMoney + amountOfMoney * numberOfYears *
          percentageOfYear / HUNDRED;
      totalProfit = totalAmount - amountOfMoney;
      alert(
          `Initial amount: ${amountOfMoney} \n
          Number of years: ${numberOfYears} \n
          Percentage of year: ${percentageOfYear} \n\n
          Total amount ${totalAmount} \n
          Total profit ${totalProfit}`);
    }
  }
}
