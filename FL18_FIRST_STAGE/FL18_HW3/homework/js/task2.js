const FIRST_ATTEMPT_PRIZE = 100;
const SECOND_ATTEMPT_PRIZE = 50;
const THIRD_ATTEMPT_PRIZE = 25;
const GUESS_RANGE_STEP = 4;
const PRIZE_MULTIPLIER = 2;
const CONFIG = {
  minRandomNumDefault: 0,
  maxRandomNumDefault: 8,
  numOfAttemptsDefault: 3,
  possiblePrizeDefault: [
    FIRST_ATTEMPT_PRIZE,
    SECOND_ATTEMPT_PRIZE,
    THIRD_ATTEMPT_PRIZE],
  totalPrizeStartDefault: 0
};

let wonPrize = false;
let isContinueGame = true;

// Returns a random number between min (inclusive) and max (inclusive)
const getRandomNum = (min, max) =>
    Math.floor(Math.random() * (max - min + 1)) + min;

function startGame() {
  let minRandomNum = CONFIG.minRandomNumDefault;
  let maxRandomNum = CONFIG.maxRandomNumDefault;
  let numOfAttempts = CONFIG.numOfAttemptsDefault;
  let possiblePrize = CONFIG.possiblePrizeDefault;
  let totalPrize = CONFIG.totalPrizeStartDefault;

  while (isContinueGame) {

    let numOfAttemptsLeft = numOfAttempts;

    for (let i = 0; i < CONFIG.numOfAttemptsDefault; i++) {

      const randomNum = getRandomNum(minRandomNum, maxRandomNum);

      let messageForUser = `Choose a roulette number from ${minRandomNum} to ${maxRandomNum} 
      \n Attempts left: ${numOfAttemptsLeft} 
      \n Total prize: ${totalPrize}$
      \n Possible prize on current attempt: ${possiblePrize[i]}$`;
      let getUserChoice = parseInt(prompt(messageForUser));

      numOfAttemptsLeft -= 1;
      if (getUserChoice === randomNum) {
        alert(
            `Congratulation, you won! Your prize is: ${possiblePrize[i]} $.`);
        totalPrize += possiblePrize[i];
        wonPrize = true;
        break;
      } else {
        wonPrize = false;
        if (isNaN(getUserChoice)) {
          isContinueGame = false;
          break;
        } else {
          alert(`You have attempts: ${numOfAttemptsLeft}`);
        }
      }
    }
    if (isContinueGame === false) {
      alert(
          `Thank you for your participation. Your prize is: ${totalPrize} $`);
      break;
    }
    if (wonPrize === false) {
      alert(
          `Thank you for your participation. Your prize is: ${totalPrize} $`);
      isContinueGame = confirm('Do you want to play again?');
      if (isContinueGame) {
        startGame();
      }
    } else {
      maxRandomNum = maxRandomNum + GUESS_RANGE_STEP;
      possiblePrize = possiblePrize.map((prize) => {
        return prize * PRIZE_MULTIPLIER;
      });

      isContinueGame = confirm('Do you want to continue?');
    }
  }
}

const newGame = () => {
  const isGameConfirmed = confirm('Do you want to play a game?');
  if (!isGameConfirmed) {
    alert('You did not become a billionaire, but can.');
  } else {
    startGame();
  }
};

newGame();