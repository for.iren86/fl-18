const SPLIT_NUM = 10;
const AGE = 18;

function reverseNumber(num) {
  let revNumber = 0;
  if (num < 0) {
    let MINUS_FOR_ADD = -1;
    num *= MINUS_FOR_ADD;
    while (num > 0) {
      revNumber = revNumber * SPLIT_NUM + num % SPLIT_NUM;
      num = Math.floor(num / SPLIT_NUM);
    }
    return revNumber * MINUS_FOR_ADD;
  } else {
    while (num > 0) {
      revNumber = revNumber * SPLIT_NUM + num % SPLIT_NUM;
      num = Math.floor(num / SPLIT_NUM);
    }
    return revNumber;
  }
}

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    func(arr[i]);
  }
}

function map(arr, func) {
  const mappedArr = [];
  forEach(arr, function(el) {
    const result = func(el);
    mappedArr.push(result);
  });
  return mappedArr;
}

function filter(arr, func) {
  const filteredArr = [];
  forEach(arr, function(el) {
    if (func(el)) {
      filteredArr.push(el);
    }
  });
  return filteredArr;
}

function getAdultAppleLovers(list) {
  return filter(list, el => {
    return el.favoriteFruit === 'apple' && el.age > AGE;
  }).map(el => el.name);
}

function getKeys(obj) {
  let keysArr = [];
  for (let key in obj) {
    if (obj[key] !== 0) {
      keysArr.push(key);
    }
  }
  return keysArr;
}

function getValues(obj) {
  let valuesArr = [];
  for (let key in obj) {
    if (obj[key] !== 0) {
      valuesArr.push(obj[key]);
    }
  }
  return valuesArr;
}

function showFormattedDate(dateObj) {
  const date = dateObj.getDate();
  const month = dateObj.toLocaleString('en-US', {month: 'short'});
  const year = dateObj.getFullYear();

  return `It is ${date} of ${month}, ${year}`;
}