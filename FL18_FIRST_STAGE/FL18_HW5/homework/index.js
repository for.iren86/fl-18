const isEquals = (a, b) => a === b;

const isBigger = (a, b) => a > b;

function storeNames() {
  let stringArr = [];
  for (let i = 0; i < arguments.length; i++) {
    stringArr.push(arguments[i]);
  }
  return stringArr;
}

const getDifference = (firstNum, secondNum) => {
  if (firstNum >= secondNum) {
    return firstNum - secondNum - firstNum;
  } else {
    return secondNum - firstNum;
  }
};

const negativeCount = (numList) => {
  let negativeNum = [];
  for (let i = 0; i < numList.length; i++) {
    if (numList[i] < 0) {
      negativeNum.push(numList[i]);
    }
  }
  return negativeNum.length;
};

const letterCount = (firstStr, secondStr) => {
  let isPositionMatched = true;
  let counter = 0;
  let idxA = 0;
  if (firstStr === '' || secondStr === '') {
    return counter;
  }
  while (idxA < firstStr.length) {
    let idxB = 0;
    while (idxB < secondStr.length) {
      let newIdx = idxA + idxB;
      if (firstStr[newIdx] !== secondStr[idxB]) {
        isPositionMatched = false;
        break;
      } else {
        isPositionMatched = true;
      }
      idxB++;
    }
    if (isPositionMatched) {
      counter += 1;
    }
    idxA++;
  }
  return counter;
};

const countPoints = (gameCollection) => {
  let scoreCounter = 0;
  const seperatedByScores = gameCollection.map(item => item.split(':'));
  const winScore = 3;
  const lostScore = 1;
  const drawScore = 0;
  for (let i = 0; i < seperatedByScores.length; i++) {
    let xTeam = parseInt(seperatedByScores[i][0]);
    let yTeam = parseInt(seperatedByScores[i][1]);
    if (xTeam > yTeam) {
      scoreCounter += winScore;
    } else if (xTeam < yTeam) {
      scoreCounter += drawScore;
    } else if (xTeam === yTeam) {
      scoreCounter += lostScore;
    }
  }
  return scoreCounter;
};