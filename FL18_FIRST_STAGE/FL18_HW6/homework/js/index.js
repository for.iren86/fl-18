function visitLink(path) {
  let curValue = localStorage.getItem(path) ?
      parseInt(localStorage.getItem(path)) :
      0;
  let newValue = curValue + 1;
  localStorage.setItem(path, newValue);
}

function viewResults() {
  let ulEl = document.createElement('ul');
  document.querySelector('#content').appendChild(ulEl);
  let elementsArray = document.getElementsByClassName('nav-link');
  for (let i = 1; i < elementsArray.length; i++) {
    let pageName = 'Page' + i;
    let item = localStorage.getItem(pageName);
    if (item) {
      let liEl = document.createElement('li');
      liEl.innerText = `You visited ${pageName} ${item} time(s)`;
      ulEl.appendChild(liEl);
    }
  }
  window.localStorage.clear();
  console.log('clear records');
}
