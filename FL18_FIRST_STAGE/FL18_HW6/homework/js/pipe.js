function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) ===
      '[object Function]';
}

const pipe = (value, ...functions) => {
  try {
    functions.forEach((func, idx) => {
      if (!isFunction(func)) {
        throw new Error(
            `Provided argument at position ${idx} is not a function!`);
      }
      value = func(value);
    });

  } catch (error) {
    return error;
  }
  return value;
};
