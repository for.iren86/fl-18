const getAge = (newDateObject) => {
  const MS_IN_YEAR = 31556952000;

  const currentDate = new Date();
  const currentAge = currentDate - Date.parse(newDateObject);
  return Math.floor(currentAge / MS_IN_YEAR);
};

const getWeekDay = (newDateObject) => {
  const today = new Date(newDateObject);
  const weekday = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'];
  return weekday[today.getDay()];
};

const getAmountDaysToNewYear = () => {
  const currentDate = new Date();
  const nextYear = currentDate.getFullYear() + 1;
  const newYearDate = new Date(nextYear, 0, 1);
  const daysLeftInMs = newYearDate - currentDate;
  const MS = 1000;
  const S = 60;
  const M = 60;
  const h = 24;
  let daysLeft = daysLeftInMs / (MS * S * M * h);
  return Math.ceil(daysLeft);
};

const getProgrammersDay = (year) => {
  const MS = 1000;
  const S = 60;
  const M = 60;
  const h = 24;
  const NUM_FOR_CONVERT_IN_MS = MS * S * M * h;
  const DAYS_TILL_HOLIDAY = 255;

  const startYear = new Date(year, 0, 1);
  const getDate = startYear.getTime();
  const daysInMs = DAYS_TILL_HOLIDAY * NUM_FOR_CONVERT_IN_MS;
  const holidayDateInMs = getDate + daysInMs;
  const holidayDate = new Date(holidayDateInMs);
  const date = holidayDate.getDate();
  const month = holidayDate.toLocaleString('en-US', {month: 'short'});
  const fullYear = holidayDate.getFullYear();
  const week = getWeekDay(holidayDate);

  return `${date} ${month}, ${fullYear} (${week})`;
};

const howFarIs = (weekDay) => {
  const today = new Date();
  const weekday = {
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday',
    7: 'Sunday'
  };
  const currentWeekDay = weekday[today.getDay()];
  const weekDayToUpper = weekDay.charAt(0).toUpperCase() + weekDay.slice(1);
  const currentWeekDayKey = Number(
      Object.keys(weekday).find(key => weekday[key] === currentWeekDay));
  const weekDayKey = Number(
      Object.keys(weekday).find(key => weekday[key] === weekDayToUpper));
  if (currentWeekDay === weekDayToUpper) {
    return `Hey, today is ${weekDayToUpper} =)`;
  } else if (currentWeekDayKey < weekDayKey) {
    const diffResult = weekDayToUpper - weekDayKey;
    return `It's ${diffResult} day(s) left till ${weekDayToUpper}`;
  } else if (currentWeekDayKey > weekDayKey) {
    const diffInAWeek = Object.keys(weekday).length - currentWeekDayKey;
    const diffResult = weekDayKey + diffInAWeek;
    return `It's ${diffResult} day(s) left till ${weekDayToUpper}`;
  }
};

const isValidIdentifier = (variable) => {
  return /^[a-zA-Z_$][a-zA-Z_$0-9]*$/.test(variable);
};

const capitalize = (testStr) => {
  return testStr.replace(/(?:^|\s)\S/g, function(a) {
    return a.toUpperCase();
  });
};

const isValidAudioFile = (fileName) => {
  return /(^[a-zA-Z]+)(.mp3|.flac|.alac|.aac)$/.test(fileName);
};

const getHexadecimalColors = (testString) => {
  return testString.match(/#([a-f0-9]{3}){1,2}\b/gi);
};

const isValidPassword = (testString) => {
  return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/g.test(testString);

};

const addThousandsSeparators = (val) => {
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

const getAllUrlsFromText = (text) => {
  const regex = new RegExp([
    '(?:(?:https?|ftp)://)(?:\\S+(?::\\S*)?@)?',
    '(?:(?!(?:10|127)(?:.\\d{1,3}){3})(?!(?:169.254|192.168)',
    '(?:.\\d{1,3}){2})(?!172.(?:1[6-9]|2\\d|3[0-1])(?:.\\d{1,3}){2})',
    '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}',
    '(?:.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)',
    '*[a-z\u00a1-\uffff0-9]+)(?:.(?:[a-z\u00a1-\uffff0-9]-*)',
    '*[a-z\u00a1-\uffff0-9]+)*(?:.(?:[a-z\u00a1-\uffff]{2,})).?)',
    '(?::\\d{2,5})?(?:[/?#]\\S*)?'].join(''), 'g');

  if (typeof text === 'undefined') {
    throw new Error('Argument for function is undefined');
  } else if (regex.test(text)) {
    return text.match(regex);
  } else if (!regex.test(text)) {
    return [];
  }
};