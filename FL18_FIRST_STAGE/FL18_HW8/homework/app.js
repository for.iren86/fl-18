const appRoot = document.getElementById('app-root');
const COMP_LESS_THAN = -1;
const COMP_GREATER_THAN = 1;
const COMP_EQUALS = 0;
const NEGATE_COMP = -1;

const mainContent = `
    <h1>Countries Search</h1>
    <div id="search-block">
      <div id="search-type"><p>Please choose the type of search:</p>
      <span id="radio-box">
          <span class="flex-item-region">
              <input id="by-region" data-element-type="byRegion" type="radio" name="choice" value="by-region">
              <label for="choice-no">By Region</label>
          </span>
          <span class="flex-item-language">
              <input id="by-language" data-element-type="byLanguage" type="radio" name="choice" value="by-language">
              <label for="by-language">By Language</label>
          </span>
      </span>
      </div>
      <div id="search-query"><p>Please choose search query:</p>
        <select id="search-query-selector" data-element-type="bySearchQuery">
          <option>Select value</option>
        </select>
      </div>
      <div class="no-choice-info" style="display: none;">
        <p>No items, please choose search query</p>
      </div>
    </div>
    <table id="main-table" style="display: none;">
      <thead>
        <tr>
          <th data-column="name" role="columnheader" data-order="asc">Country name</th>
          <th role="columnheader">Capital</th>
          <th role="columnheader">World Region</th>
          <th role="columnheader">Languages</th>
          <th data-column="area" role="columnheader" data-order="none">Area</th>
          <th role="columnheader">Flag</th>
        </tr>
      </thead>  
      <tbody id="table-content">
      </tbody>
    </table>
`;

const isRegionSelected = () => {
  const el = document.querySelector('*[data-element-type="byRegion"]');
  return !!el && el.checked;
};
const isLanguageSelected = () => {
  const el = document.querySelector('*[data-element-type="byLanguage"]');
  return !!el && el.checked;
};

const setAscOrdered = (el) => el.setAttribute('data-order', 'asc');
const setDescOrdered = (el) => el.setAttribute('data-order', 'desc');

const isAscOrdered = (el) => el.getAttribute('data-order') === 'asc';
const isDescOrdered = (el) => el.getAttribute('data-order') === 'desc';
const isNoneOrdered = (el) => el.getAttribute('data-order') === 'none';

const orderByColumnAsc = (data, selectedColumn) => {
  return data.sort((a, b) => {
    if (a[selectedColumn] < b[selectedColumn]) {
      return COMP_LESS_THAN;
    } else if (a[selectedColumn] > b[selectedColumn]) {
      return COMP_GREATER_THAN;
    }
    return COMP_EQUALS;
  });
};

function orderByColumnDesc(data, selectedColumn) {
  return data.sort((a, b) => {
    let res;
    if (a[selectedColumn] < b[selectedColumn]) {
      res = COMP_LESS_THAN;
    } else if (a[selectedColumn] > b[selectedColumn]) {
      res = COMP_GREATER_THAN;
    } else {
      res = COMP_EQUALS;
    }
    return res * NEGATE_COMP;
  });
}

const orderDesc = (selectedColumn, selectedValue) => {
  let data;
  if (isRegionSelected()) {
    data = externalService.getCountryListByRegion(selectedValue);
  } else if (isLanguageSelected()) {
    data = externalService.getCountryListByLanguage(selectedValue);
  }

  data = orderByColumnAsc(data, selectedColumn);
  renderTable(data);
};

const orderAsc = (selectedColumn, selectedValue) => {
  let data;
  if (isRegionSelected()) {
    data = externalService.getCountryListByRegion(selectedValue);
  } else if (isLanguageSelected()) {
    data = externalService.getCountryListByLanguage(selectedValue);
  }

  data = orderByColumnDesc(data, selectedColumn);
  renderTable(data);
};

const onSortClick = (e) => {
  const target = e.target;
  const selectedColumn = target.getAttribute('data-column');

  console.log(e);
  if (!selectedColumn) {
    return;
  }

  const selectedValue = document.querySelector('#search-query-selector').value;

  if (isAscOrdered(target)) {
    orderDesc(selectedColumn, selectedValue);
    setDescOrdered(target);
  } else if (isDescOrdered(target)) {
    orderAsc(selectedColumn, selectedValue);
    setAscOrdered(target);
  } else if (isNoneOrdered(target)) {
    orderDesc(selectedColumn, selectedValue);
    setDescOrdered(target);
  }
};

const showTable = () => {
  document.querySelector('.no-choice-info').
      setAttribute('style', 'display: none');
  document.querySelector('#main-table').
      setAttribute('style', 'display: block');
};

const hideTable = () => {
  document.querySelector('.no-choice-info').
      setAttribute('style', 'display: block');
  document.querySelector('#main-table').
      setAttribute('style', 'display: none');
};

const renderOptions = (data) => {
  const optionHtmlPattern = '<option>{VALUE}</option>';

  const options = document.querySelector(
      '*[data-element-type="bySearchQuery"]');
  let optionsHTML = optionHtmlPattern.replaceAll('{VALUE}', 'Select value');

  data.map(region => optionHtmlPattern.replaceAll('{VALUE}', region)).
      forEach(el => {
        optionsHTML += el;
      });
  options.innerHTML = optionsHTML;
  hideTable();
};

const renderTable = (data) => {
  const tableContent = document.querySelector('#table-content');
  let tableHtmlContent = '';

  for (let i = 0; i < data.length; i++) {
    const item = data[i];
    tableHtmlContent += `<tr data-id="${i}" class='row>'><td>${item['name']}</td>
<td>${item['capital']}</td><td>${item['region']}</td><td>${Object.values(
        item.languages).join(', ')}</td>
    <td>${item['area']}</td><td><img src=${item['flagURL']} alt='flag'/></td></tr>`;
  }
  tableContent.innerHTML = tableHtmlContent;
  showTable();
};

const onRegionSelected = () => renderOptions(externalService.getRegionsList());
const onLanguageSelected = () => renderOptions(
    externalService.getLanguagesList());
const onSearchSelected = (e) => {
  const target = e.target;
  const value = target.value;

  let data;
  if (isRegionSelected()) {
    data = externalService.getCountryListByRegion(value);
  } else if (isLanguageSelected()) {
    data = externalService.getCountryListByLanguage(value);
  }

  renderTable(data);
};

window.onload = function() {
  appRoot.innerHTML = mainContent;
  document.querySelector('*[data-element-type="byRegion"]').
      addEventListener('click', e => onRegionSelected(e));
  document.querySelector('*[data-element-type="byLanguage"]').
      addEventListener('click', e => onLanguageSelected(e));
  document.querySelector('*[data-element-type="bySearchQuery"]').
      addEventListener('change', e => onSearchSelected(e));

  for (const el of document.querySelectorAll('*[role="columnheader"]')) {
    el.addEventListener('click', e => onSortClick(e));
  }
};
