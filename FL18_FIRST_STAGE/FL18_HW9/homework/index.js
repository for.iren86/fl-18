const row = document.getElementsByTagName('tr');
const cells = document.querySelectorAll('td');

cells.forEach(cell => {
  cell.addEventListener('click', event => {
    const rowIdx = cell.closest('tr').rowIndex;
    const colIdx = cell.cellIndex;
    let target = event.target;
    const rowNodes = row[rowIdx].childNodes;

    if (target.textContent !== 'Special Cell' && colIdx !== 0) {
      if (target.className === 'highlight-yellow') {
        highlightElement(target, 'highlight-white');
      } else {
        highlightElement(target, 'highlight-yellow');
      }
    }

    if (colIdx === 0 && target.textContent !== 'Special Cell') {
      for (let i = 0; i < rowNodes.length; i++) {
        if (rowNodes[i].nodeName.toLowerCase() === 'td'
            && rowNodes[i].className !== 'highlight-yellow') {
          if (rowNodes[i].className === 'highlight-blue') {
            highlightElement(rowNodes[i], 'highlight-white');
          } else {
            highlightElement(rowNodes[i], 'highlight-blue');
          }
        }
      }
    }

    if (target.textContent === 'Special Cell') {
      cells.forEach(cell => {
        if (cell.tagName.toLowerCase() === 'td' && cell.className !==
            'highlight-yellow' && cell.className !== 'highlight-blue') {
          if (cell.className === 'highlight-green') {
            highlightElement(cell, 'highlight-white');
          } else {
            highlightElement(cell, 'highlight-green');
          }
        }
      });
    }
  });
});

function highlightElement(el, value) {
  el.setAttribute('class', value);
}

const PHONE_PATTERN = '+380*********';

const getPhoneInput = () => {
  return document.querySelector('#phone');
};

const fetInfoBlock = () => {
  return document.querySelector('.alert-info');
};

const getErrorBlock = () => {
  return document.querySelector('.alert-error');
};

const getSubmitButton = () => {
  return document.querySelector('form button');
};

getPhoneInput().addEventListener('input', function() {

  if (!getPhoneInput().checkValidity()) {
    getSubmitButton().disabled = true;
    fetInfoBlock().style.visibility = 'hidden';
    getErrorBlock().style.visibility = 'visible';
    getErrorBlock().innerHTML = `Type number does not follow format ${PHONE_PATTERN}`;
  } else {
    getSubmitButton().disabled = false;
  }
});

getSubmitButton().addEventListener('click', function() {

  if (!getPhoneInput().checkValidity()) {
    getSubmitButton().disabled = true;
    fetInfoBlock().style.visibility = 'hidden';
    getErrorBlock().style.visibility = 'visible';
    getErrorBlock().innerHTML = `Type number does not follow format ${PHONE_PATTERN}`;
  } else {
    getSubmitButton().disabled = false;
    getErrorBlock().style.visibility = 'hidden';
    fetInfoBlock().style.visibility = 'visible';
    fetInfoBlock().innerHTML = 'Data was successfully sent';
  }

  if (getPhoneInput().value === '') {
    getSubmitButton().disabled = false;
  }
});

const teamA = {
  lftX: 440,
  rgtX: 470,
  topY: 210,
  btmY: 240
};

const teamB = {
  lftX: 958,
  rgtX: 988,
  topY: 210,
  btmY: 240
};

const getCourtElement = () => {
  return document.querySelector('#court');
};

const getBallElement = () => {
  return document.querySelector('#ball');
};

const moveBall = (goalLeft, goalTop) => {
  getBallElement().style.left = `${goalLeft}px`;
  getBallElement().style.top = `${goalTop}px`;
};

const getScoreTeamAElement = () => {
    return document.querySelector('.score-team-A');
};

const getScoreTeamBElement = () => {
  return document.querySelector('.score-team-B');
};

let scoreTeamA = 0;
let scoreTeamB = 0;

getScoreTeamAElement().textContent = `Team A:${scoreTeamA}`;
getScoreTeamBElement().textContent = `Team B:${scoreTeamB}`;

const notificationMessage = document.createElement('p');
document.querySelector('.notification').appendChild(notificationMessage);

function hideMessage() {
  notificationMessage.innerHTML = '';
}

const TIMEOUT_IN_MS = 3000;

getCourtElement().addEventListener('click', function(event) {

  event.preventDefault()

  const getCourtPosition = getCourtElement().getBoundingClientRect();
  const court = getCourtElement();
  const ball = getBallElement();
  const BALL_MIDDLE_NUM = 2;
  const getBallPosition = {
    top: event.clientY - getCourtPosition.top - court.clientTop - ball.clientHeight / BALL_MIDDLE_NUM,
    left: event.clientX - getCourtPosition.left - court.clientLeft - ball.clientWidth / BALL_MIDDLE_NUM
  };

  if (getBallPosition.top < 0) {
    getBallPosition.top = 0;
  }
  if (getBallPosition.left < 0) {
    getBallPosition.left = 0;
  }
  if (getBallPosition.left + ball.clientWidth > court.clientWidth) {
    getBallPosition.left = court.clientWidth - ball.clientWidth;
  }
  if (getBallPosition.top + ball.clientHeight > court.clientHeight) {
    getBallPosition.top = court.clientHeight - ball.clientHeight;
  }

  moveBall(getBallPosition.left, getBallPosition.top);

  if (event.clientX <= teamA.rgtX && event.clientX >= teamA.lftX
      && event.clientY <= teamA.btmY && event.clientY >= teamA.topY) {
    scoreTeamA += 1;
    getScoreTeamAElement().textContent = `Team A:${scoreTeamA}`;
    notificationMessage.textContent = 'Team A score!';
    notificationMessage.style.color= 'blue';
    setTimeout(hideMessage, TIMEOUT_IN_MS);
  } else if (event.clientX <= teamB.rgtX && event.clientX >= teamB.lftX
      && event.clientY <= teamB.btmY && event.clientY >= teamB.topY) {
    scoreTeamB += 1;
    getScoreTeamBElement().textContent = `Team B:${scoreTeamB}`;
    notificationMessage.textContent = 'Team B score!';
    notificationMessage.style.color= 'red';
    setTimeout(hideMessage, TIMEOUT_IN_MS);
  }
});
