import {
    drawWinMessage,
    playerOTurnMessage,
    playerOWinMessage,
    playerXTurnMessage,
    playerXWinMessage,
    scoreMessagePattern
} from './messages';

let playerXTurn = true;
let stepNumber = 0;

let playerXWins = 0;
let playerOWins = 0;
const gameGrid = [0, 1, 2, 3, 4, 5, 6, 7, 8];

const getPlayerScoreMessage = (score) => {
    return scoreMessagePattern.replace('{PLAYER_SCORE}', score);
};

const selectPlayer = () => {
    playerXTurn = Math.random() < 0.5;
    if (playerXTurn) {
        document.getElementById('turn').innerHTML = playerXTurnMessage;
    } else {
        document.getElementById('turn').innerHTML = playerOTurnMessage;
    }
};

const isPlayerXTurn = () => {
    return playerXTurn;
};

const findWinnerCells = () => {
    if (gameGrid[0]===gameGrid[3] && gameGrid[3]===gameGrid[6]) {
        return [0, 3, 6];
    }
    if (gameGrid[0]===gameGrid[1] && gameGrid[1]===gameGrid[2]) {
        return [0, 1, 2];
    }
    if (gameGrid[0]===gameGrid[4] && gameGrid[4]===gameGrid[8]) {
        return [0, 4, 8];
    }
    if (gameGrid[1]===gameGrid[4] && gameGrid[4]===gameGrid[7]) {
        return [1, 4, 7];
    }
    if (gameGrid[2]===gameGrid[5] && gameGrid[5]===gameGrid[8]) {
        return [2, 5, 8];
    }
    if (gameGrid[2]===gameGrid[4] && gameGrid[4]===gameGrid[6]) {
        return [2, 4, 6];
    }
    if (gameGrid[3]===gameGrid[4] && gameGrid[4]===gameGrid[5]) {
        return [3, 4, 5];
    }
    if (gameGrid[6]===gameGrid[7] && gameGrid[7]===gameGrid[8]) {
        return [6, 7, 8];
    }
    return [];
};

const isDraw = () => {
    return stepNumber===9;
};

const onPlayerXTurnComplete = (event) => {
    let gridPos = event.target.id;
    document.getElementById(gridPos).innerHTML = 'X';
    document.getElementById('turn').innerHTML = playerOTurnMessage;
    gameGrid[gridPos] = -1;
    stepNumber++;
    playerXTurn = false;
};

const onPlayerOTurnComplete = (event) => {
    let gridPos = event.target.id;
    document.getElementById(gridPos).innerHTML = 'O';
    document.getElementById('turn').innerHTML = playerXTurnMessage;
    gameGrid[gridPos] = -2;
    stepNumber++;
    playerXTurn = true;
};

const updatePlayerXWins = () => {
    playerXWins++;

    document.getElementById('turn').innerHTML = playerXWinMessage;
    document.getElementById('scoreX').innerHTML = getPlayerScoreMessage(playerXWins);
};

const updatePlayerOWins = () => {
    playerOWins++;

    document.getElementById('turn').innerHTML = playerOWinMessage;
    document.getElementById('scoreO').innerHTML = getPlayerScoreMessage(playerOWins);
};

const updateDrawWins = () => {
    updatePlayerXWins();
    updatePlayerOWins();
    document.getElementById('turn').innerHTML = drawWinMessage;
};

const resetGridPositions = () => {
    for (let i = 0; i < gameGrid.length; i++) {
        gameGrid[i] = i;
    }
};

const removeOnCellClicks = () => {
    const grid = document.querySelectorAll('.grid-box');
    for (const gridCell of grid) {
        gridCell.removeEventListener('click', onCellClick);
    }
};

const assignOnCellClicks = () => {
    const grid = document.querySelectorAll('.grid-box');
    for (const gridCell of grid) {
        gridCell.addEventListener('click', onCellClick, {once: true});
        gridCell.innerHTML = '';
    }
};

const highlightWinnerCells = (winnerCells) => {
    for (const i of winnerCells) {
        const cellEl = document.querySelector(`div.grid-box[id="${i}"]`);
        cellEl.style.backgroundColor = "green";
    }
};

const resetCellsHighlight = () => {
    for (let i = 0; i < gameGrid.length; i++) {
        const cellEl = document.querySelector(`div.grid-box[id="${i}"]`);
        cellEl.style.backgroundColor = "steelblue";
    }
};

const updateIfWinner = (playerXTurnNow) => {
    const winnerCells = findWinnerCells();
    const isNoWinners = winnerCells.length===0;

    if (isNoWinners && isDraw()) {
        updateDrawWins();
        removeOnCellClicks();
        return;
    }

    if (isNoWinners) {
        return;
    }

    if (playerXTurnNow) {
        updatePlayerXWins();
    } else {
        updatePlayerOWins();
    }

    removeOnCellClicks();
    highlightWinnerCells(winnerCells);
};

const onCellClick = (event) => {
    if (isPlayerXTurn()) {
        onPlayerXTurnComplete(event);
        updateIfWinner(true);
    } else {
        onPlayerOTurnComplete(event);
        updateIfWinner(false);
    }
};

const startNewGame = () => {
    stepNumber = 0;
    selectPlayer();

    removeOnCellClicks();
    resetGridPositions();
    assignOnCellClicks();
    resetCellsHighlight();
};

const reset = () => {
    playerXWins = 0;
    playerOWins = 0;

    document.getElementById('scoreX').innerHTML = getPlayerScoreMessage(playerXWins);
    document.getElementById('scoreO').innerHTML = getPlayerScoreMessage(playerOWins);

    startNewGame();
};

const assignListeners = () => {
    document.getElementById('newGameBtn').onclick = () => startNewGame();
    document.getElementById('resetBtn').onclick = () => reset();
};

window.onload = () => {
    startNewGame();
    assignListeners();
};
