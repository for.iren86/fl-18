const playerXTurnMessage = `<span class="h3">Player 1 turn</span>`;
const playerOTurnMessage = `<span class="h3">Player 2 turn</span>`;

const playerXWinMessage = '<span class="h3">Player 1 won!</span>';
const playerOWinMessage = '<span class="h3">Player 2 won!</span>';
const drawWinMessage = '<span class="h3">Draw!</span>';

const scoreMessagePattern = '<span class="h4">{PLAYER_SCORE}</span>';

export {
    playerXTurnMessage,
    playerOTurnMessage,
    playerXWinMessage,
    playerOWinMessage,
    drawWinMessage,
    scoreMessagePattern
};
