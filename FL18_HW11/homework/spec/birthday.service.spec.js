const BirthdayService = require('../src/birthday.service');

describe('Birthday module', () => {

  let birthdayServ;
  let todayDate;
  let todayTimestamp;

  beforeAll(() => {
    birthdayServ = new BirthdayService();
    todayDate = new Date('2040-02-16');
    todayTimestamp = new Date('2040-02-16').getTime();
  });

  it('assert future birthday', async() => {
    const expectedDaysDifference = 29;
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-03-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert past birthday', async() => {
    const expectedDaysDifference = -92;
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-11-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert today equal to birthday', async() => {
    const expectedDaysDifference = 0;
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-02-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert error for wrong argument type', async() => {
    return expectAsync(
        birthdayServ.howLongToMyBirthday('2050-02-16', todayDate))
        .toBeRejectedWith(new Error('Wrong argument!'));
  });

  it('assert timestamp future birthday', async() => {
    const expectedDaysDifference = 29;
    const birthdayTimestampDate = 2531001600000;
    const result = await birthdayServ.howLongToMyBirthday(birthdayTimestampDate,
        todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert timestamp past birthday', async() => {
    const expectedDaysDifference = -92;
    const birthdayTimestampDate = 2552169600000;
    const result = await birthdayServ.howLongToMyBirthday(birthdayTimestampDate,
        todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert timestamp today equal to timestamp birthday', async() => {
    const expectedDaysDifference = 0;
    const birthdayTimestampDate = 2212963200000;
    const result = await birthdayServ.howLongToMyBirthday(birthdayTimestampDate,
        todayTimestamp);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert console output for today equal to birthday', async() => {
    console.log = jasmine.createSpy('log');
    await birthdayServ.howLongToMyBirthday(
        new Date('2050-02-16'), todayDate);
    expect(console.log).toHaveBeenCalledWith('Hooray!!! It is today!');
  });

  it('assert console output for timestamp today equal to timestamp birthday',
      async() => {
        console.log = jasmine.createSpy('log');
        const birthdayTimestampDate = 2212963200000;
        await birthdayServ.howLongToMyBirthday(birthdayTimestampDate, todayTimestamp);
        expect(console.log).toHaveBeenCalledWith('Hooray!!! It is today!');
      });

  it('assert console output for future birthday', async() => {
    console.log = jasmine.createSpy('log');
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-03-16'), todayDate);
    expect(console.log).toHaveBeenCalledWith(
        `Soon...Please, wait just ${result} day/days`);
  });

  it('assert console output for past birthday', async() => {
    console.log = jasmine.createSpy('log');
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-11-16'), todayDate);
    expect(console.log).toHaveBeenCalledWith(
        `Oh, you have celebrated it ${Math.abs(
            result)} day/s ago, don't you remember?`);
  });

  it('assert console output for timestamp future birthday', async() => {
    const birthdayTimestampDate = 2531001600000;
    console.log = jasmine.createSpy('log');
    const result = await birthdayServ.howLongToMyBirthday(
        birthdayTimestampDate, todayDate);
    expect(console.log).toHaveBeenCalledWith(
        `Soon...Please, wait just ${result} day/days`);
  });

  it('assert console output for timestamp past birthday', async() => {
    const birthdayTimestampDate = 2552169600000;
    console.log = jasmine.createSpy('log');
    const result = await birthdayServ.howLongToMyBirthday(
        birthdayTimestampDate, todayDate);
    expect(console.log).toHaveBeenCalledWith(
        `Oh, you have celebrated it ${Math.abs(
            result)} day/s ago, don't you remember?`);
  });

  it('assert future birthday', async() => {
    const expectedDaysDifference = 29;
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-03-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert future months difference', () => {
    const expectedDaysDifference = 1;
    const result = birthdayServ.monthDifference(
        new Date('2040-03-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert past days difference', () => {
    const expectedDaysDifference = 29;
    const result = birthdayServ.daysDifference(
        new Date('2040-03-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert past timestamp days difference', async() => {
    const expectedDaysDifference = 29;
    const birthdayTimestampDate = 2215468800000;
    const result = await birthdayServ.daysDifference(
        birthdayTimestampDate, todayTimestamp);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert birthday and today gap more than 6 months in future', async() => {
    const expectedDaysDifference = 31;
    const todayDate = new Date('2030-12-16');
    const result = await birthdayServ.howLongToMyBirthday(
        new Date('2050-01-16'), todayDate);
    expect(result).toBe(expectedDaysDifference);
  });

  it('assert console output for birthday and today gap more than 6 months in future',
      async() => {
        const todayDate = new Date('2030-12-16');
        console.log = jasmine.createSpy('log');
        const result = await birthdayServ.howLongToMyBirthday(
            new Date('2050-01-16'), todayDate);
        expect(console.log).toHaveBeenCalledWith(
            `Soon...Please, wait just ${result} day/days`);
      });

  it('assert today equal to birthday with default argument', async() => {
    const expectedDaysDifference = 0;
    const birthday = new Date();
    const result = await birthdayServ.howLongToMyBirthday(birthday);
    expect(result).toBe(expectedDaysDifference);
  });
});
