const getBiggestNum = require('../src/get-bigest-number');

describe('Biggest number module', () => {

  it('get the biggest number in an established size array', () => {
    const expectedMaxNum = 102;
    const result = getBiggestNum([1, 2, 3, 100, 101, 102, 0, -1, -2, -3]);
    expect(result).toBe(expectedMaxNum);
  });

  it('get an error for smaller array size', () => {
    expect(() => {
      getBiggestNum([1]);
    }).toThrow(new Error('Not enough arguments'));
  });

  it('get an error for bigger array size', () => {
    expect(() => {
      getBiggestNum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
    }).toThrow(new Error('Too many arguments'));
  });

  it('get an error for incorrect argument type', () => {
    expect(() => {
      getBiggestNum(['1', 2, '3']);
    }).toThrow(new Error('Wrong argument type'));
  });

  it('get an error for empty array', () => {
    expect(() => {
      getBiggestNum([]);
    }).toThrow(new Error('Not enough arguments'));
  });

  it('get the biggest number among equal values in array', () => {
    const expectedMaxNum = 1;
    const result = getBiggestNum([1, 1, 1]);
    expect(result).toBe(expectedMaxNum);
  });

  it('get the biggest number among equal zero values in array', () => {
    const expectedMaxNum = 0;
    const result = getBiggestNum([0, 0]);
    expect(result).toBe(expectedMaxNum);
  });
});
