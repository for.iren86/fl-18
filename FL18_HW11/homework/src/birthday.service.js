const MS = 1000;
const SEC = 60;
const MIN = 60;
const HOUR = 24;
const MS_IN_DAY = MS * SEC * MIN * HOUR;
const NUM_OF_MONTHS_IN_A_YEAR = 12;

class BirthdayService {
  convertToUtcDay(date) {
    let newDate = date;
    if (typeof date === 'number') {
      newDate = new Date(date);
    }
    return new Date(Date.UTC(newDate.getUTCFullYear(),
        newDate.getUTCMonth(), newDate.getUTCDate()));
  }

  monthDifference(date1, date2) {
    let months =
        (date1.getFullYear() - date2.getFullYear()) * NUM_OF_MONTHS_IN_A_YEAR;
    months += date1.getMonth() - date2.getMonth();
    return months;
  }

  daysDifference(date1, date2) {
    return (date1.valueOf() - date2.valueOf()) / MS_IN_DAY;
  }

  log(msg) {
    console.log(msg);
  }

  calculateDays(date, today) {
    today = this.convertToUtcDay(today);

    const currentYearBirthday = this.convertToUtcDay(date);
    currentYearBirthday.setFullYear(today.getUTCFullYear());
    const pastYearBirthday = new Date(
        Date.UTC(currentYearBirthday.getUTCFullYear() - 1,
            currentYearBirthday.getUTCMonth(),
            currentYearBirthday.getUTCDate()));
    const nextYearBirthday = new Date(
        Date.UTC(currentYearBirthday.getUTCFullYear() + 1,
            currentYearBirthday.getUTCMonth(),
            currentYearBirthday.getUTCDate()));

    if (today.valueOf() === currentYearBirthday.valueOf()) {
      this.congratulateWithBirthday();
      return 0;
    }

    const currentYearVsTodayMonthDiff = this.monthDifference(
        currentYearBirthday, today);
    const pastYearVsTodayMonthDiff = this.monthDifference(pastYearBirthday,
        today);
    const nextYearVsTodayMonthDiff = this.monthDifference(nextYearBirthday,
        today);

    const minMonthDiff =
        Math.min(Math.abs(currentYearVsTodayMonthDiff),
            Math.abs(pastYearVsTodayMonthDiff),
            Math.abs(nextYearVsTodayMonthDiff));

    let daysDiff = -1;
    if (minMonthDiff === Math.abs(currentYearVsTodayMonthDiff)) {
      daysDiff = this.daysDifference(currentYearBirthday, today);
    } else if (minMonthDiff === Math.abs(pastYearVsTodayMonthDiff)) {
      daysDiff = this.daysDifference(pastYearBirthday, today);
    } else if (minMonthDiff === Math.abs(nextYearVsTodayMonthDiff)) {
      daysDiff = this.daysDifference(nextYearBirthday, today);
    }

    this.notifyWaitingTime(daysDiff);

    return daysDiff;
  }

  howLongToMyBirthday(date, today = new Date()) {
    return new Promise((resolve, reject) => {
      const ms = 100;

      setTimeout(() => {
        const isValid = date instanceof Date
            || typeof date === 'number';
        if (!isValid) {
          return reject(new Error('Wrong argument!'));
        }

        return resolve(this.calculateDays(date, today));
      }, ms);
    });
  }

  congratulateWithBirthday() {
    this.log('Hooray!!! It is today!');
  }

  notifyWaitingTime(daysDiff) {
    if (daysDiff < 0) {
      this.log(
          `Oh, you have celebrated it ${Math.abs(
              daysDiff)} day/s ago, don't you remember?`);
    } else {
      this.log(`Soon...Please, wait just ${daysDiff} day/days`);
    }
  }
}

module.exports = BirthdayService;
