const MAX_LENGTH = 10;
const MIN_LENGTH = 2;

const getBigestNumber = (array) => {
  const arrLength = [...array].length;

  if (arrLength > MAX_LENGTH) {
    throw new Error('Too many arguments');
  }
  if (arrLength < MIN_LENGTH) {
    throw new Error('Not enough arguments');
  }
  for (const num of array) {
    if (typeof num !== 'number') {
      throw new Error('Wrong argument type');
    }
  }
  return Math.max(...array);
};

module.exports = getBigestNumber;
