'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
function Pizza(size, type) {

  const expectedNumOfArgs = 2;

  if (arguments.length !== expectedNumOfArgs) {
    throw new PizzaException(
        `Required two arguments, given: ${arguments.length}`
    );
  }
  const isAllowedSize =
      Pizza.allowedSizes.some(
          allowedSize => allowedSize.name === size.name);
  if (!isAllowedSize) {
    throw new PizzaException('Invalid size');
  }
  const isAllowedType =
      Pizza.allowedTypes.some(
          allowedType => allowedType.name === type.name);
  if (!isAllowedType) {
    throw new PizzaException('Invalid type');
  }

  this.size = size;
  this.type = type;
  this.extraIngredients = [];
}

Pizza.SIZE_S = {name: 'SMALL', price: 50};
Pizza.SIZE_M = {name: 'MIDDLE', price: 75};
Pizza.SIZE_L = {name: 'LARGE', price: 100};

Pizza.TYPE_VEGGIE = {name: 'VEGGIE', price: 50};
Pizza.TYPE_MARGHERITA = {name: 'MARGHERITA', price: 60};
Pizza.TYPE_PEPPERONI = {name: 'MARGHERITA', price: 70};

Pizza.EXTRA_TOMATOES = {name: 'EXTRA_TOMATOES', price: 5};
Pizza.EXTRA_CHEESE = {name: 'EXTRA_CHEESE', price: 7};
Pizza.EXTRA_MEAT = {name: 'EXTRA_MEAT', price: 9};

Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];

Pizza.allowedTypes = [
  Pizza.TYPE_VEGGIE,
  Pizza.TYPE_MARGHERITA,
  Pizza.TYPE_PEPPERONI];

Pizza.allowedExtraIngredients = [
  Pizza.EXTRA_TOMATOES,
  Pizza.EXTRA_CHEESE,
  Pizza.EXTRA_MEAT];

Pizza.prototype.getSize = function() {
  return this.size;
};

Pizza.prototype.getPrice = function() {
  return this.size.price + this.type.price +
      this.getIngredientPrices().reduce((a, b) => a + b, 0);
};

const getIngredientNames = function() {
  return this.extraIngredients.map(ingredient => ingredient.name);
};

const getIngredientPrices = function() {
  return this.extraIngredients.map(ingredient => ingredient.price);
};

Pizza.prototype.getExtraIngredients = function() {
  return this.extraIngredients;
};

Pizza.prototype.getIngredientNames = getIngredientNames;

Pizza.prototype.getIngredientPrices = getIngredientPrices;

Pizza.prototype.getPizzaInfo = function() {
  const ingredientNames = this.getIngredientNames();
  const price = this.getPrice();
  return `Size: ${this.size.name}, type: ${this.type.name}; 
  extra ingredients: ${ingredientNames}; price: ${price}UAH.`;
};

Pizza.prototype.addExtraIngredient = function(ingredient) {
  if (arguments.length !== 1) {
    throw new PizzaException(
        `Required one argument, given: ${arguments.length}`
    );
  }
  if (ingredient === undefined) {
      throw new PizzaException('Invalid ingredient');
  }

  const isAllowedIngredient = Pizza.allowedExtraIngredients.some(
      allowedIngredient => allowedIngredient.name === ingredient.name);

  const isIngredientExists = this.extraIngredients.some(
      allowedIngredient => allowedIngredient.name === ingredient.name);

  if (!isAllowedIngredient) {
    throw new PizzaException('Invalid ingredient');
  }
  if (isIngredientExists) {
    throw new PizzaException('Duplicate ingredient');
  }
  this.extraIngredients.push(ingredient);
};

Pizza.prototype.removeExtraIngredient = function(ingredient) {
  if (ingredient === undefined) {
    throw new PizzaException('Invalid ingredient');
  }

  const isAllowedIngredient = Pizza.allowedExtraIngredients.some(
      allowedIngredient => allowedIngredient.name === ingredient.name);

  const isIngredientExists = this.extraIngredients.some(
      allowedIngredient => allowedIngredient.name === ingredient.name);

  if (!isAllowedIngredient) {
    throw new PizzaException('Invalid ingredient');
  }
  if (!isIngredientExists) {
    throw new PizzaException(`No such ingredient: ${ingredient}`);
  }
  this.extraIngredients = this.extraIngredients.filter(
      currentIngredient => currentIngredient.name !== ingredient.name);
};

/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
function PizzaException(log) {
  this.log = log;
}