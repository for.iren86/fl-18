const url = 'https://jsonplaceholder.typicode.com/users';
let usersData = [];
let selectedUser = null;
const SPINNER_TIMEOUT = 1000;

const mainHtmlFragmentPattern = `
    <tr>
      <td style="padding-right: 3em; border: 0.1em solid black;">{ID-VALUE}</td>
      <td style="padding-right: 3em; border: 0.1em solid black;">{USERNAME-VALUE}</td>
      <td style="padding-right: 3em; border: 0.1em solid black;">{EMAIL-VALUE}</td>
      <td>
        <button class="edit-button" data-item-id="{ID-VALUE}" 
            style="width: 5em; height: 2em; color: #000; background-color: green;">
            Edit</button>
      </td>
      <td>
        <button class="delete-button" data-item-id="{ID-VALUE}" 
            style="width: 5em; height: 2em; color: #000; background-color: red;">
            Delete</button>
      </td>
    </tr>
    `;

const fetchApiCall = (onFetchComplete) => {
  fetch(url).
      then(response => {
        if (!response.ok) {
          throw new Error('Could not reach website.');
        }
        return response.json();
      }).
      then(data => onFetchComplete(data, null)).
      catch(error => onFetchComplete(null, error));
};

const updateApiCall = (user, onFetchComplete) => {
  fetch(`${url}/${user.id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  }).
      then(response => {
        if (!response.ok) {
          throw new Error('Could not reach website.');
        }
        return response.json();
      }).
      then(data => onFetchComplete(data, null)).
      catch(error => onFetchComplete(null, error));
};

const deleteApiCall = (userId, onFetchComplete) => {
  fetch(`${url}/${userId}`, {
    method: 'DELETE'
  }).
      then(response => {
        if (!response.ok) {
          throw new Error('Could not reach website.');
        }
        return response.json();
      }).
      then(data => onFetchComplete(data, null)).
      catch(error => onFetchComplete(null, error));
};

const selectUser = (userId) => {
  selectedUser = usersData.find(data => data.id.toString() === userId);
  const userIdInput = document.querySelector('#user-id');
  const usernameInput = document.querySelector('#username');
  const emailInput = document.querySelector('#email');
  userIdInput.value = selectedUser.id;
  usernameInput.value = selectedUser.username;
  emailInput.value = selectedUser.email;
};

const resetUser = () => {
  selectedUser = null;
  const userIdInput = document.querySelector('#user-id');
  const usernameInput = document.querySelector('#username');
  const emailInput = document.querySelector('#email');
  userIdInput.value = '';
  usernameInput.value = '';
  emailInput.value = '';
};

const renderUsersList = (users) => {
  const tableRows = users.map(user => mainHtmlFragmentPattern.
      replaceAll('{ID-VALUE}', user.id).
      replaceAll('{USERNAME-VALUE}', user.username).
      replaceAll('{EMAIL-VALUE}', user.email));
  document.querySelector('.table-body').innerHTML =
      tableRows.reduce((prev, cur) => prev + cur, '');
};

const showSpinner = () => {
  const spinner = document.getElementById('spinner');
  spinner.classList.add('show');
  setTimeout(() => {
    spinner.classList.remove('show');
  }, SPINNER_TIMEOUT);
};

const hideSpinner = () => {
  const spinner = document.getElementById('spinner');
  spinner.classList.remove('show');
};

const fetchData = (onFetchComplete) => {
  showSpinner();
  fetchApiCall((data, error) => {
    if (!error) {
      usersData = data;
      renderUsersList(usersData);
      onFetchComplete();
    }
    hideSpinner();
  });
};

const onEditClick = (event) => {
  const userId = event.target.getAttribute('data-item-id');
  selectUser(userId);
};

const onDeleteClick = (event) => {
  showSpinner();
  const userId = event.target.getAttribute('data-item-id');
  deleteApiCall(userId, (data, error) => {
    hideSpinner();
    if (!error) {
      usersData = usersData.filter(user => user.id.toString() !== userId);
      renderUsersList(usersData);
      assignListeners();
    }
  });
};

const onUpdateClick = (event) => {
  showSpinner();
  event.preventDefault();
  const username = document.querySelector('#username');
  const userEmail = document.querySelector('#email');
  const updatedUserInput = Object.assign({}, selectedUser);
  updatedUserInput.username = username.value;
  updatedUserInput.email = userEmail.value;
  updateApiCall(updatedUserInput, (data, error) => {
    hideSpinner();
    if (!error) {
      selectedUser.username = data.username;
      selectedUser.email = data.email;
      resetUser();
      renderUsersList(usersData);
      assignListeners();
    }
  });
};

const assignListeners = () => {
  const editButtons = document.querySelectorAll('.edit-button');
  const deleteButtons = document.querySelectorAll('.delete-button');
  const form = document.querySelector('.user-form');
  Array.from(editButtons).
      forEach(el => el.addEventListener('click', onEditClick));
  Array.from(deleteButtons).
      forEach(el => el.addEventListener('click', onDeleteClick));
  form.addEventListener('submit', onUpdateClick);
};

window.onload = () => {
  fetchData(() => assignListeners());
};