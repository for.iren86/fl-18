const socket = new WebSocket('ws://localhost:8080/ws');
let userName;

const messageBlockPattern = `
    <div class="message-block {BLOCK-POSITION-VALUE}"> 
        <span class="user-name-element">{USERNAME-VALUE}</span>
        <span class="message-element">{MESSAGE-VALUE}</span>
        <span class="time-element">{TIME-VALUE}</span>
    </div>
    `;

const addMessage = (receivedMessage) => {
  const messagesList = document.querySelector('#messages');
  let messageBlock = messageBlockPattern.
      replace('{USERNAME-VALUE}', receivedMessage.username).
      replace('{MESSAGE-VALUE}', receivedMessage.message).
      replace('{TIME-VALUE}', receivedMessage.time);
  if (receivedMessage.username === userName) {
    messageBlock = messageBlock.
        replace('{BLOCK-POSITION-VALUE}', 'right-side');
  } else {
    messageBlock = messageBlock.
        replace('{BLOCK-POSITION-VALUE}', 'left-side');
  }
  messagesList.innerHTML += messageBlock;
};

socket.onmessage = event => {
  const receivedMessage = event.data;
  addMessage(JSON.parse(receivedMessage));
};

const onSendMessage = (event) => {
  event.preventDefault();
  const date = new Date();
  const currentTime = date.toLocaleString('en-US',
      {hour: 'numeric', minute: 'numeric', hour12: true}
  );
  const messageText = document.querySelector('.message-input').value;
  let message = JSON.stringify(
      {username: userName, message: messageText, time: currentTime}
  );
  if (messageText) {
    socket.send(message);
    const localMessage = {username: userName, message: messageText, time: currentTime};
    document.querySelector('.message-input').value = '';
    addMessage(localMessage);
  }
};

const main = () => {
  userName = prompt('What\'s your name?');
  const submitButton = document.querySelector('.submit');
  submitButton.addEventListener('click', onSendMessage);
};

window.onload = () => {
  main();
};