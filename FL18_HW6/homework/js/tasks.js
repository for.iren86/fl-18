const arr = ['1', '3', '4', '2', '5'];

const getMaxEvenElement = (myArr) => {
  const newArr = myArr.filter(el => {
    const divideNum = 2;
    return el % divideNum === 0;
  });
  return Math.max(...newArr);
};

console.log(getMaxEvenElement(arr));

let a = 3;
let b = 5;

[a, b] = [b, a];

console.log(a);
console.log(b);

const getValue = (value) => {
  return value ?? '-';
};

console.log(getValue(0));
console.log(getValue(4));
console.log(getValue('Some Text'));
console.log(getValue(null));
console.log(getValue(undefined));

const arrayOfArrays = [
  ['name', 'dan'],
  ['age', '21'],
  ['city', 'Lviv']
];

const getObjFromArray = array => Object.fromEntries(array);

console.log(getObjFromArray(arrayOfArrays));

const obj1 = {name: 'nick'};

const addUniqueId = obj => {
  return {...obj, id: Symbol()};
};

console.log(addUniqueId(obj1));
console.log(addUniqueId({name: 'Buffy'}));
console.log(Object.keys(obj1).includes('id'));

const oldObj = {
  name: 'willow',
  details: {id: 1, age: 47, university: 'LNU'}
};

const getRegroupedObject = (obj) => {
  const {university, ...user} = obj.details;
  user.firstName = obj.name;
  return {university, user};
};

console.log(getRegroupedObject(oldObj));

const arr2 = [2, 3, 4, 2, 4, 'a', 'c', 'a'];

const getArrayWithUniqueElements = (arr) => [...new Set(arr)];

console.log(getArrayWithUniqueElements(arr2));

const phoneNumber = '0123456789';

const hideNumber = (num) => {
  const restOfNum = -4;
  const last4Digits = num.slice(restOfNum);
  return last4Digits.padStart(num.length, '*');
};

console.log(hideNumber(phoneNumber));

function required(param) {
  throw new Error(`${param} is required.`);
}

const add = (a = required('a'), b = required('b')) => {
  return a + b;
};

console.log(add(2,3));

function* generateIterableSequence() {
  yield 'I';
  yield 'Love';
  yield 'EPAM';
}

const generatorObject = generateIterableSequence();

for (let value of generatorObject) {
  console.log(value);
}