const currWindow = window;
let expression = '';
let result = 0;
const operatorsList = ['/', '*', '+', '-'];
const LAST_INDEX = -1;

const logLine = `
  <div class="log-line" data-id="{VALUE-ID}">
    <span class="log-line-circle"></span>
    <span class="log-line-expression">{RESULT-VALUE}</span>
    <i class="log-line-icon">&#x2715;</i>
  </div>
`;

const onClearClick = (event) => {
  event.preventDefault();
  setDefaultValue();
};

const onButtonClick = (event) => {
  event.preventDefault();
  setValue(event);
};

const onCalculateClick = (event) => {
  event.preventDefault();
  displayResult(calculate());
};

const enableDisplay = (event) => {
  event.preventDefault();
  $('#display').removeAttr('disabled');
};

const setDefaultValue = () => {
  const displayField = $('#display');
  displayField.val('0');
};

$(document).ready(() => {
  setDefaultValue();
  $('#clear').on('click', onClearClick);
  $('input').on('click', onButtonClick);
  $('input[name="doit"]').on('click', onCalculateClick);
  $('.interactive-button').on('click', enableDisplay);
});

const isLastSymbolIsOperator = (val) => {
  return operatorsList.includes(val.slice(LAST_INDEX));
};

const setValue = (event) => {
  enableDisplay(event);
  handleError();
  const displayField = $('#display');
  displayField.css('color', 'black');

  if (event.target.name !== 'clear' && event.target.name !== 'doit') {
    let currentValue = displayField.val();
    const userInput = event.target.value;
    const userInputIsOperator = operatorsList.includes(userInput);

    const inputLength = currentValue.length;
    if (inputLength === 1 && currentValue === '-' && userInputIsOperator) {
      return;
    }
    if (currentValue === '0') {
      if (!userInputIsOperator || userInput === '-') {
        displayField.val(userInput);
      }
    } else if (isLastSymbolIsOperator(currentValue) &&
        userInputIsOperator) {
      const lastSymbol = currentValue.slice(LAST_INDEX);
      displayField.val(currentValue.replace(lastSymbol, userInput));
    } else {
      displayField.val(currentValue + userInput);
    }
  }
};

const getUserInput = () => {
  const completedExpression = $('#display').val();
  expression = completedExpression;
  return completedExpression;
};

const calculate = () => {
  const currentUserInput = getUserInput();
  if (isLastSymbolIsOperator(expression)) {
    throw new Error('Unexpected end of input');
  }
  const res = currWindow.eval(currentUserInput);
  if (res === Infinity) {
    $('#display').val('ERROR').css('color', 'red');
    throw new Error('Division by "0" is not allowed');
  } else {
    result = res.toString();
    return res;
  }
};

const displayResult = (res) => {
  const resValue = $('#display').val(res);
  addLogLine();
  return resValue;
};

function* nextIdGenerator() {
  let index = 1;

  while (true) {
    yield index++;
  }
}

const getNextId = nextIdGenerator();

const addLogLine = () => {
  const elementId = getNextId.next().value;
  const logExpr = `${expression}=${result}`;
  let currLogLine = logLine.replace('{RESULT-VALUE}', logExpr);

  currLogLine = currLogLine
      .replace('{VALUE-ID}', elementId.toString());
  $('.log-block').prepend(currLogLine);
  $(`.log-line[data-id="${elementId}"] .log-line-circle`)
      .on('click', makeLineActive);
  $(`.log-line[data-id="${elementId}"] .log-line-icon`)
      .on('click', removeLogLine);
  highlightParticularLor(elementId);
};

const makeLineActive = (event) => {
  event.preventDefault();
  $(event.target).toggleClass('active');
};

const highlightParticularLor = (elementId) => {
  const expressionElement = $(`.log-line[data-id="${elementId}"] .log-line-expression`);
  const expressionsValue = expressionElement.text();
  const isLogExprContains = expressionsValue.includes('48');
  if (isLogExprContains) {
    expressionElement.addClass('underline');
  }
};

const removeLogLine = (event) => {
  event.preventDefault();
  const removeElement = event.target;
  removeElement.parentNode.remove();
};

const handleError = () => {
  const displayField = $('#display');
  if (displayField.val() === 'ERROR') {
    displayField.css('color', 'black').val('0');
  }
};

$(() => {
  $('.log-block').scroll(() => {
    let scrollPos = $('.log-block').scrollTop();
    console.log(`Scroll Top: `, scrollPos);
  });
});
