const addButtonsEventListener = (startNewGameClb, startGameClb) => {
    const resetLink = document.querySelector('.reset-link');
    resetLink.addEventListener('click', startNewGameClb);

    const buttons = document.querySelectorAll('button');
    buttons.forEach(button => button.addEventListener('click', startGameClb));
};

export {addButtonsEventListener};