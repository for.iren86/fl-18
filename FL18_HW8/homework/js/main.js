import {addButtonsEventListener} from './listeners';
import {showEqualMessage, showLostMessage, showResultMessage, showWonMessage} from './messages';

const gameEndScore = 3;

const buttonNames = ['Rock', 'Paper', 'Scissors'];
let userScore = {'won': 0, 'lost': 0};
let gameCurrNum = 0;

const getUserChoice = (event) => {
    return event.target.textContent;
};

const getBotChoice = () => {
    const randomIndex = Math.floor(Math.random() * buttonNames.length);
    return buttonNames[randomIndex];
};

const isUserWon = (userValue, randomValue) => {
    const rules = {
        'Scissors': 'Paper',
        'Paper': 'Rock',
        'Rock': 'Scissors'
    };

    return Object.entries(rules)
    .some(rule => rule[0]===userValue && rule[1]===randomValue);
};

const makeAllButtonsDisabled = () => {
    const buttons = document.querySelectorAll('button');
    buttons.forEach(button => button.disabled = true);
};

const startGame = (event) => {
    const userChoice = getUserChoice(event);
    const botChoice = getBotChoice();
    if (userScore.won!==gameEndScore && userScore.lost!==gameEndScore) {
        gameCurrNum += 1;
        if (userChoice===botChoice) {
            showEqualMessage(gameCurrNum, userChoice, botChoice);
        } else if (isUserWon(userChoice, botChoice)) {
            userScore.won += 1;
            showWonMessage(gameCurrNum, userChoice, botChoice);
        } else {
            userScore.lost += 1;
            showLostMessage(gameCurrNum, userChoice, botChoice);
        }
    }
    if (userScore.won===gameEndScore) {
        makeAllButtonsDisabled();
        showResultMessage(true);
    }
    if (userScore.lost===gameEndScore) {
        makeAllButtonsDisabled();
        showResultMessage();
    }
};

const startNewGame = () => {
    location.reload();
};

window.onload = () => {
    addButtonsEventListener(startNewGame, startGame);
};
