let resultLinePattern = `<div class="result-line">{RESULT-VALUE}</div>`;

const showWonMessage = (gameCurrNum, userValue, randomValue) => {
    const messBlock = document.querySelector('.result-block');
    const wonMessage = `Round ${gameCurrNum}, ${userValue} vs. ${randomValue}, You've WON!`;
    messBlock.innerHTML = resultLinePattern.replace('{RESULT-VALUE}', wonMessage);
};

const showLostMessage = (gameCurrNum, userValue, randomValue) => {
    const messBlock = document.querySelector('.result-block');
    const lostMessage = `Round ${gameCurrNum}, ${userValue} vs. ${randomValue}, You've LOST!`;
    messBlock.innerHTML = resultLinePattern.replace('{RESULT-VALUE}', lostMessage);
};

const showEqualMessage = (gameCurrNum, userValue, randomValue) => {
    const messBlock = document.querySelector('.result-block');
    const equalMessage = `Round ${gameCurrNum}, ${userValue} vs. ${randomValue}, You were EQUAL!`;
    messBlock.innerHTML = resultLinePattern.replace('{RESULT-VALUE}', equalMessage);
};

const showResultMessage = (userWon = false) => {
    const messBlock = document.querySelector('.result-block');
    const userMessage = 'You WON!';
    const randomizerMessage = 'Randomizer WON!';
    if (userWon) {
        messBlock.innerHTML += `<div class="result-line">${userMessage}</div>`;
    } else {
        messBlock.innerHTML += `<div class="result-line">${randomizerMessage}</div>`;
    }
};

export {showWonMessage, showLostMessage, showEqualMessage, showResultMessage};
