const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
  entry: ['./js/main.js', './scss/main.scss'],
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    watchFiles: ['./*'],
    host: 'localhost',
    liveReload: true
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()]
  },
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Game',
      template: './index.html'
    }),
    new MiniCssExtractPlugin({
      filename: './dist/bundle.css'
    }),
    new ESLintPlugin(
        {
          extensions: [`js`],
          exclude: [
            `/node_modules/`
          ]
        }
    )
  ]
};
