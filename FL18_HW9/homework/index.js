const RESET_TO_INITIAL_STATE_TIMEOUT = 60000;

class Magazine {
  constructor() {
    this.state = new ReadyForPushNotification(this);
    this.articles = {};
    this.followers = {};
  }

  registerNewArticle(topic, article) {
    if (!this.articles[topic]) {
      this.articles[topic] = [];
    }
    this.articles[topic].push(article);
  }

  addArticle(staffMember, article) {
    this.state.addArticle(staffMember, article);
  }

  approve(staffMember) {
    this.state.approve(staffMember);
  }

  publish(staffMember) {
    this.state.publish(staffMember);
  }

  isEnoughArticles() {
    const minSize = 5;
    const aggregatedSize = Object.values(this.articles)
        .map(list => list.length)
        .reduce((prev, cur) => prev + cur, 0);
    return aggregatedSize === minSize;
  }

  changeState(state) {
    this.state = state;
  }

  subscribeTo(topic, follower) {
    if (!this.followers[topic]) {
      this.followers[topic] = [];
    }
    this.followers[topic].push(follower);
  }

  unsubscribe(topic, follower) {
    if (this.followers[topic]) {
      this.followers[topic] = this.followers[topic]
          .filter(
              currFollower => currFollower.getName() !== follower.getName());
    }
  }

  notifyFollowers() {
    Object.entries(this.followers).forEach(arr => {
      const topic = arr[0];
      const followersList = arr[1];
      const articleList = this.articles[topic];
      const dataPattern = '{article} {follower}';
      articleList.forEach(article => {
        followersList.forEach(follower => {
          const data = {
            'text': dataPattern
                .replace('{article}', article)
                .replace('{follower}', follower.getName())
          };
          follower.onUpdate(data);
        });
      });
    });
  }
}

class State {

  constructor(magazine) {
    this.magazine = magazine;
  }
}

class ReadyForPushNotification extends State {
  addArticle(staffMember, article) {
    this.magazine.registerNewArticle(staffMember.getTopic(), article);
    if (this.magazine.isEnoughArticles()) {
      this.magazine.changeState(new ReadyForApprove(this.magazine));
    }
  }

  approve(staffMember) {
    if (!staffMember.isRoleManager()) {
      console.log('you do not have permissions to do it');
    } else {
      console.log(
          `Hello ${staffMember.getName()}. You can't approve. We don't have enough of publications`);
    }
  }

  publish(staffMember) {
    console.log(
        `Hello ${staffMember.getName()}. You can't publish. We are creating publications now.`);
  }
}

class ReadyForApprove extends State {

  approve(staffMember) {
    if (this.magazine.isEnoughArticles() && staffMember.isRoleManager()) {
      console.log(`Hello ${staffMember.getName()} You've approved the changes`);
      this.magazine.changeState(new ReadyForPublish(this.magazine));
    } else if (!this.magazine.isEnoughArticles()) {
      console.log(
          `Hello ${staffMember.getName()}. You can't approve. We don't have enough of publications`);
    } else if (!staffMember.isRoleManager()) {
      console.log('you do not have permissions to do it');
    }
  }

  publish(staffMember) {
    console.log(
        `Hello ${staffMember.getName()} You can't publish. We don't have a manager's approval.`);
  }
}

class ReadyForPublish extends State {
  scheduleResetToInitialState() {
    setTimeout(() => {
      this.magazine.changeState(new ReadyForPushNotification(this.magazine));
    }, RESET_TO_INITIAL_STATE_TIMEOUT);
  }

  publish(staffMember) {
    console.log(
        `Hello ${staffMember.getName()} You've recently published publications.`);
    this.magazine.changeState(new PublishInProgress(this.magazine));
    this.magazine.notifyFollowers();
    this.scheduleResetToInitialState();
  }

  approve(staffMember) {
    if (!staffMember.isRoleManager()) {
      console.log('you do not have permissions to do it');
    } else {
      console.log(
          `Hello ${staffMember.getName()} Publications have been already approved by you.`);
    }
  }
}

class PublishInProgress extends State {
  approve(staffMember) {
    if (!staffMember.isRoleManager()) {
      console.log('you do not have permissions to do it');
    } else {
      console.log(
          `Hello ${staffMember.getName()}. While we are publishing we can't do any actions`);
    }
  }

  publish(staffMember) {
    console.log(
        `Hello ${staffMember.getName()}. While we are publishing we can't do any actions.`);
  }
}

class MagazineEmployee {
  constructor(name, topic, magazine) {
    this.name = name;
    this.topic = topic;
    this.magazine = magazine;
  }

  getName() {
    return this.name;
  }

  isRoleManager() {
    return this.topic === 'manager';
  }

  getTopic() {
    return this.topic;
  }

  addArticle(article) {
    this.magazine.addArticle(this, article);
  }

  approve() {
    this.magazine.approve(this);
  }

  publish() {
    this.magazine.publish(this);
  }
}

class Follower {
  constructor(name) {
    this.name = name;
  }

  getName() {
    return this.name;
  }

  subscribeTo(magazine, topic) {
    magazine.subscribeTo(topic, this);
  }

  unsubscribe(magazine, topic) {
    magazine.unsubscribe(topic, this);
  }

  onUpdate(data) {
    console.log(data['text']);
  }
}

const magazine = new Magazine();
const manager = new MagazineEmployee('Andrii', 'manager', magazine);
const sport = new MagazineEmployee('Serhii', 'sport', magazine);
const politics = new MagazineEmployee('Volodymyr', 'politics', magazine);
const general = new MagazineEmployee('Olha', 'general', magazine);

const iryna = new Follower('Iryna');
const maksym = new Follower('Maksym');
const mariya = new Follower('Mariya');

iryna.subscribeTo(magazine, 'sport');
maksym.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'general');

sport.addArticle('something about sport');
politics.addArticle('something about politics');
general.addArticle('some general information');
politics.addArticle('something about politics again');
sport.approve();
manager.approve();
politics.publish();
sport.addArticle('news about sport');
manager.approve();
sport.publish();
manager.approve('news about sport');

iryna.unsubscribe(magazine, 'sport');
